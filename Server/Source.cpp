#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <cstring>
#include <winsock2.h> 
#include <fstream>
#pragma comment(lib, "WS2_32.lib")
#include <sstream>
#include <map>
#include <deque>

constexpr int PORT = 5555;
const int MAX_CLIENTS = 5;
const std::string filename = "C:/Users/sasha/Documents/clients.txt"; // Хранение клиентов(не нужно)
const std::string filenameMessage = "C:/Users/sasha/Documents/message.txt";// Хранение сообщений(не нужно)
std::deque<std::string> messageHistory; // Хранение истории сообщений
class Client {
public:
    Client(int id, std::string username) : id(id), username(username) {}

    int getId() const {
        return id;
    }

    std::string getUsername() const {
        return username;
    }

private:
    int id;
    std::string username;
};

std::vector<int> clientSockets;
std::vector<Client> clients;

void loadMessageFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return;
    }

    std::string line;
    int id = 0;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string username;
        iss >> username;
        messageHistory.push_back(username);
    }

    file.close();
}



void saveMessagesToFile(const std::string& filename) {
    std::ofstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return;
    }

    for (const std::string& message : messageHistory) {
        file << message << std::endl;
    }

    file.close();
}

void clientHandler(int clientSocket) {
    char buffer[1024] = { 0 };

    int clientId = -1;
    for (size_t i = 0; i < clientSockets.size(); ++i) {
        if (clientSockets[i] == clientSocket) {
            clientId = i;
            break;
        }
    }

    if (clientId == -1) {
        std::cerr << "Client not found in clients vector" << std::endl;
        return;
    }

    std::string username = clients[clientId].getUsername();

    while (true) {
        int valread = recv(clientSocket, buffer, 1024, 0);
        if (valread == 0) {
            std::cout << "Client " << username << " disconnected" << std::endl;
            break;
        }
        std::string receivedMessage = std::string(buffer);
        size_t pos1 = receivedMessage.find("message");
        // Показать последние 10 сообщений клиенту
        if (pos1 != std::string::npos) {
            for (const std::string& message : messageHistory) {
                send(clientSocket, message.c_str(), message.length(), 0);
            }
        }
        else {
        // Анализ сообщения и отправка конкретному адресату
            size_t pos = receivedMessage.find(' ');
            if (pos != std::string::npos) {
                std::string recipient = receivedMessage.substr(0, pos); // Имя никнейма адресата
                std::string messageToSend = "private message from " + username + ": " + receivedMessage.substr(pos + 1);

                // Отправка сообщения только адресату
                for (size_t i = 0; i < clientSockets.size(); ++i) {
                    if (clients[i].getUsername() == recipient) {
                        send(clientSockets[i],messageToSend.c_str(), messageToSend.length(), 0);
                        break;
                    }
                    else {
                        send(clientSockets[i], ("CHAT." + username + ":" + receivedMessage).c_str(), valread, 0);
                        messageHistory.push_back(username + ":" + receivedMessage);
                        if (messageHistory.size() > 10) {
                            messageHistory.pop_front();
                        }
                        saveMessagesToFile("message.txt");
                        break;
                    }
                    
                }
            }
            else {
                // Рассылка сообщения всем клиентам, включая отправителя
                for (size_t i = 0; i < clientSockets.size(); ++i) {
                    send(clientSockets[i], ("CHAT." + username + ":" + receivedMessage).c_str(), valread, 0);
                }
            }
                messageHistory.push_back(username + ":" + receivedMessage);
                if (messageHistory.size() > 10) {
                    messageHistory.pop_front();
                }
                saveMessagesToFile("message.txt");
            }
        }
    }


void loadClientsFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return;
    }

    std::string line;
    int id = 0;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string username;
        iss >> username;
        clients.emplace_back(id++, username);
    }

    file.close();
}

void saveClientsToFile(const std::string& filename) {
    std::ofstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return;
    }

    for (const auto& client : clients) {
        file << client.getUsername() << std::endl;
    }

    file.close();
}

int main() {
   // loadClientsFromFile("clients.txt");
  //  loadMessageFromFile("message.txt");
    WSADATA WSAData; 
    WSAStartup(MAKEWORD(2, 0), &WSAData);

    int serverSocket, newSocket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Creating socket file descriptor
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    int enable = 1;
    // Binding socket to port
    if (bind(serverSocket, (struct sockaddr*)&address, sizeof(address)) == SOCKET_ERROR) {
        std::cout << "Bind function failed with error: " << WSAGetLastError() << std::endl;
        return -1;

    }

    // Прослушивание подключений
    if (listen(serverSocket, 3) < 0) {
        perror("Listen failed");
            exit(EXIT_FAILURE);
    }

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if ((newSocket = accept(serverSocket, (struct sockaddr*)&address, &addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        std::cout << "Client connected" << std::endl;

        char username[50] = {0};
        recv(newSocket, username, 50, 0);
        clients.emplace_back(clients.size(), username);
        clientSockets.push_back(newSocket);

        std::thread t(clientHandler, newSocket);
        std::cout << "Client add" << std::endl;
        t.detach();
        saveClientsToFile("clients.txt");
    }

    WSACleanup();
    return 0;
}