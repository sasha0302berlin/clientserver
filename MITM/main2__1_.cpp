#include <stdio.h>
#include <pcap.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <thread>

#define SNAP_LEN 1518
#define ETH_ALEN 6
#define ETH_HLEN 14
#define IP_HLEN 20
const char* interface_vmnet2 = "ens37";
const char* interface_vmnet3 = "ens38";
std::map<std::string, std::pair<std::string, time_t>>commutation_table;  

// Функция для пересылки пакетов 

uint16_t ip_checksum(const char *buf, int len); 

unsigned short calculate_tcp_checksum(const struct iphdr *iph, unsigned short *packet); 

void clearCommTable(){  
    time_t current_time;  
    while(true){  
        time(&current_time);  
        for (auto it = commutation_table.cbegin(); it != commutation_table.cend();){  
            if ((current_time - it->second.second) >= 60){   
                g_lock.lock();  
                commutation_table.erase(it++);  
                g_lock.unlock();  
            }  
            else  
            it++;  
        }  
        std::this_thread::sleep_for(std::chrono::seconds(10));   
    }  
}  
// Функция для пересылки пакетов
uint16_t ip_checksum(const char *buf, int len);
unsigned short calculate_tcp_checksum(const struct iphdr *iph, unsigned short *packet);
uint16_t recalculate_tcp_checksum(const char* packet, int packet_len,
                                  uint32_t initial_sum) {
    struct iphdr* iph = (struct iphdr*)packet;
    struct tcphdr* tcph = (struct tcphdr*)(packet + iph->ihl * 4);

    // 1. Вычисляем сумму заголовка IP
    uint32_t sum = initial_sum; // Сумма, полученная из предыдущего вычисления
    sum += iph->saddr + iph->daddr + htons(iph->protocol) +
           htons(iph->tot_len);

    // 2. Вычисляем сумму заголовка TCP, исключая поле контрольной суммы
    sum += tcph->source + tcph->dest + htons(tcph->th_off << 2) +
           htons(tcph->th_flags & ~TH_PUSH); // Убираем флаг PSH

    // 3. Вычисляем сумму данных TCP
    int tcp_data_len = packet_len - (iph->ihl * 4) - (tcph->th_off << 2); // Длина данных TCP
    const char* tcp_data = packet + (iph->ihl * 4) + (tcph->th_off << 2);  // Указатель на данные TCP

    // Вычисляем сумму данных TCP по 2 байта
    for (int i = 0; i < tcp_data_len; i += 2) {
        sum += *(uint16_t*)(tcp_data + i);
    }

    // 4. Если длина данных TCP нечетная, добавляем последний байт
    if (tcp_data_len % 2) {
        sum += *(uint8_t*)(tcp_data + tcp_data_len - 1);
    }

    // 5. Сводим сумму к 16 бит
    while (sum >> 16) {
        sum = (sum & 0xFFFF) + (sum >> 16);
    }

    // 6. Возвращаем дополнение до 1 от полученной суммы
    return ~(sum & 0xFFFF);
}

uint16_t recalculate_ip_checksum(const char* packet, int packet_len) {
    struct iphdr* iph = (struct iphdr*)packet;

    // 1. Вычисляем сумму заголовка IP
    uint32_t sum = 0;
    sum += iph->saddr + iph->daddr + htons(iph->protocol) +
           htons(iph->tot_len);

    // 2. Вычисляем сумму данных IP
    const char* ip_data = packet + iph->ihl * 4; // Указатель на данные IP
    int ip_data_len = packet_len - iph->ihl * 4; // Длина данных IP

    // Вычисляем сумму данных IP по 2 байта
    for (int i = 0; i < ip_data_len; i += 2) {
        sum += *(uint16_t*)(ip_data + i);
    }

    // 3. Если длина данных IP нечетная, добавляем последний байт
    if (ip_data_len % 2) {
        sum += *(uint8_t*)(ip_data + ip_data_len - 1);
    }

    // 4. Сводим сумму к 16 бит
    while (sum >> 16) {
        sum = (sum & 0xFFFF) + (sum >> 16);
    }

    // 5. Возвращаем дополнение до 1 от полученной суммы
    return ~(sum & 0xFFFF);
}

void forward_packet(const u_char* packet, int packet_len, pcap_t* dst_handle) {
    // Извлечение заголовков IP и TCP
    struct ether_header *eth_header = (struct ether_header*) packet;
    struct iphdr *iph = (struct iphdr*)(packet);
    uint16_t ethertype = ntohs(eth_header->ether_type);

    if (ethertype == ETHERTYPE_IP) { // IPv4
        struct ip *ip_header = (struct ip *) (packet + sizeof(struct ether_header));

        if (ip_header->ip_p == IPPROTO_TCP) { // TCP protocol

            struct tcphdr *tcph = (struct tcphdr *) (packet + 14 + 20);
            if(tcph->psh == 1) {
                tcph->psh = 0;
                uint32_t initial_sum = 0; // Инициализируем начальную сумму 0
                uint16_t new_checksum = recalculate_tcp_checksum((char *) packet, packet_len, initial_sum);
                std::cout << new_checksum;
                // Обновляем контрольную сумму в TCP-заголовке
                tcph->check = new_checksum;
                uint16_t new_ip_checksum = recalculate_ip_checksum((char *)packet, packet_len);

                // Обновляем контрольную сумму в IP-заголовке
                ((struct iphdr*)packet)->check = new_ip_checksum;
            }
            if (pcap_inject(dst_handle, packet, packet_len) == -1) {
                std::cerr << "Ошибка при отправке пакета: " << pcap_geterr(dst_handle) << std::endl;
            }
            return; // Не TCP-пакет, выходим из функции

        }}
    if (pcap_inject(dst_handle, packet, packet_len) == -1) {
        std::cerr << "Ошибка при отправке пакета: " << pcap_geterr(dst_handle) << std::endl;
    }



}

// Поток для захвата и пересылки пакетов
void capture_and_forward(const char* src_interface, const char* dst_interface) {
    char errbuf[PCAP_ERRBUF_SIZE];

    // Открытие устройства для захвата
    pcap_t* src_handle = pcap_open_live(src_interface, BUFSIZ, 1, 1, errbuf);
    if (src_handle == nullptr) {
        std::cerr << "Ошибка при открытии устройства " << src_interface << ": " << errbuf << std::endl;
        return;
    }
    if (pcap_setdirection(src_handle, PCAP_D_IN) == -1) {
        std::cerr << "Could not set direction for capturing" << std::endl;
        return;
    }
    // Открытие устройства для отправки
    pcap_t* dst_handle = pcap_open_live(dst_interface, BUFSIZ, 0, 1, errbuf);
    if (dst_handle == nullptr) {
        std::cerr << "Ошибка при открытии устройства " << dst_interface << ": " << errbuf << std::endl;
        pcap_close(src_handle);
        return;
    }

    // Захват и пересылка пакетов
    pcap_loop(src_handle, 0, [](u_char* user, const struct pcap_pkthdr* header, const u_char* packet) {
        pcap_t* dst_handle = reinterpret_cast<pcap_t*>(user);
        forward_packet(packet, header->caplen, dst_handle);
    }, reinterpret_cast<u_char*>(dst_handle));

    // Закрытие устройств
    pcap_close(src_handle);
    pcap_close(dst_handle);
}

int main() {
    // Запуск потоков для захвата и пересылки
    std::thread vmnet2_thread(capture_and_forward, interface_vmnet2, interface_vmnet3);
    std::thread vmnet3_thread(capture_and_forward, interface_vmnet3, interface_vmnet2);

    // Ожидание завершения потоков
    vmnet2_thread.join();
    vmnet3_thread.join();

    return 0;
}