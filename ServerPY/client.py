import asyncio


async def receive_message(reader):
    while True:
        data = await reader.read(100)
        message = data.decode()
        print(message)


async def send_message(writer):
    while True:
        message = input()
        writer.write(message.encode())
        await writer.drain()


async def tcp_client():
    reader, writer = await asyncio.open_connection('127.0.0.1', 5555)

    username = input("Введите ваше имя пользователя: ")
    writer.write(username.encode())
    await writer.drain()

    asyncio.create_task(receive_message(reader))
    await send_message(writer)


asyncio.run(tcp_client())
