import asyncio
import re

PORT = 5555
ADDRESS = '127.0.0.1'
MAX_MESSAGES = 10
messages = []
clients = dict()


def find_client(username):
    if username in clients:
        return clients[username]
    return None


async def handle_client(reader, writer):
    username = (await reader.read(100)).decode().strip()
    # username = re.sub(b'\x00*$', b'', username).decode().strip()
    print(f"{username} присоединился к чату.")
    clients[username] = writer

    for msg in messages:
        writer.write(f"{msg}".encode())
        await writer.drain()
 
    while True:
        data = await reader.read(100)
        message = data.decode().strip()
        print(message)
        message_words = message.split()
        to_username = message_words[0] if len(message_words) > 1 and message_words[0] in clients else None
        if message_words[0] == "<message":
            for msg in messages[-min(MAX_MESSAGES, len(messages)):]:
                writer.write(f"{msg}\n".encode())
                await writer.drain()
        elif to_username:
            private_msg = ' '.join(message_words[1:])
            clients[to_username].write(f"Privat message from {username}: {message_words[1:]}\n".encode())
            await clients[to_username].drain()
        else:
            message = f"{username}: {message}"
            messages.append(message)
            print(message.encode())
            for client_user in clients:
                clients[client_user].write(message.encode())
                await clients[client_user].drain()


async def main():
    server = await asyncio.start_server(handle_client, ADDRESS, PORT)

    addr = server.sockets[0].getsockname()
    print(f'Сервер запущен на {addr}')

    async with server:
        await server.serve_forever()


asyncio.run(main())
