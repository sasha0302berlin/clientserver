#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <cstring>
#include <winsock2.h>
#include <fstream>
#pragma comment(lib, "WS2_32.lib")
#include <sstream>
#include <Ws2tcpip.h>
#include <locale.h>


constexpr int BUFSIZE = 1024;
const int PORT = 5555;
const char* SERVER_IP = "127.0.0.1";
const std::string enter = "Введите ваше сообщение:";

void flush_stdin()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}


DWORD WINAPI clientReceive(LPVOID lpParam) { //Получение данных от сервера
    char buffer[BUFSIZE] = { 0 };
    SOCKET server = *(SOCKET*)lpParam;
    while (true) {
        SSIZE_T len = recv(server, buffer, sizeof(buffer), 0);
        if (len == SOCKET_ERROR) {
            std::cout << "recv function failed with error: " << WSAGetLastError() << std::endl;
            return -1;
        }
        if (strcmp(buffer, "exit") == 0) {
            std::cout << "Server disconnected." << std::endl;
            return 1;
        }
        std::cout << buffer << std::endl;
        std::cout << enter << std::endl;
        memset(buffer, 0, sizeof(buffer));
    }
    return 1;
}

DWORD WINAPI clientSend(LPVOID lpParam) { //Отправка данных на сервер
    char buffer[BUFSIZE] = { 0 };
    SOCKET server = *(SOCKET*)lpParam;
    size_t len;

    while (true) {
        fgets(buffer, BUFSIZE, stdin);
        len = strlen(buffer);
        if (send(server, buffer, len-1, 0) == SOCKET_ERROR) {
            std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
            return -1;
        }
        if (strcmp(buffer, "exit") == 0) {
            std::cout << "Thank you for using the application" << std::endl;
            break;
        }
    }
    return 1;
}
int main() {
    WSADATA WSAData;
    WSAStartup(MAKEWORD(2, 0), &WSAData);
    int clientSocket;
    struct sockaddr_in serverAddress;
    setlocale(LC_ALL, "Russian");
    // Creating socket file descriptor
    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, SERVER_IP, &serverAddress.sin_addr) <= 0) {
        perror("Invalid address/ Address not supported");
        exit(EXIT_FAILURE);
    }
    // Connect to server
    if (connect(clientSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }
    std::cout << "Подключён к чату" << std::endl;

    char username[50];
    std::cout << "Введите ваше имя: ";
    std::cin >> username;
    send(clientSocket, username, strlen(username), 0);

    DWORD tid;
    HANDLE t1 = CreateThread(NULL, 0, clientReceive, &clientSocket, 0, &tid);
    if (t1 == NULL) std::cout << "Thread creation error: " << GetLastError();
    HANDLE t2 = CreateThread(NULL, 0, clientSend, &clientSocket, 0, &tid);
    if (t2 == NULL) std::cout << "Thread creation error: " << GetLastError();

    WaitForSingleObject(t1, INFINITE);
    WaitForSingleObject(t2, INFINITE);

    closesocket(clientSocket);
    WSACleanup();

}